package src;

import java.util.ArrayList;

public abstract class JogoDeDoisJogadores {
    String nomeJogo;
    String nomeJogador1;
    String nomeJogador2;
    int numeroDeRodadas;
    ArrayList<Integer> historicoResultados;

    public JogoDeDoisJogadores(
        String nomeJogo, 
        String nomeJogador1, 
        String nomeJogador2, 
        int numeroDeRodadas
    ) {
        this.nomeJogo = nomeJogo;
        this.nomeJogador1 = nomeJogador1;
        this.nomeJogador2 = nomeJogador2;
        this.numeroDeRodadas = numeroDeRodadas;
    }

    protected abstract String executarRodadaDoJogo();

    public String getNomeJogo() {
        return nomeJogo;
    }

    public String getNomeJogador1() {
        return nomeJogador1;
    }

    public String getNomeJogador2() {
        return nomeJogador2;
    }

    public int getNumeroDeRodadas() {
        return numeroDeRodadas;
    }

    public String jogar() {
        String vencedorDaRodada;
        int vitoriasJ1 = 0;
        int vitoriasJ2 = 0;

        for(int i=0; i<numeroDeRodadas; i++){
            vencedorDaRodada = executarRodadaDoJogo();
            if(vencedorDaRodada == nomeJogador1) {
                vitoriasJ1++;
            } else if(vencedorDaRodada == nomeJogador2) {
                vitoriasJ2++;
            }
        }
        if(vitoriasJ1 == vitoriasJ2) {
            return "O jogo terminou em empate após "+numeroDeRodadas+" rodadas.";
        }
        return String.format(
            "O jogador %s venceu o jogo por %d a %d", 
            vitoriasJ1 > vitoriasJ2 ? nomeJogador1 : nomeJogador2,
            vitoriasJ1 > vitoriasJ2 ? vitoriasJ1 : vitoriasJ2,
            vitoriasJ1 > vitoriasJ2 ? vitoriasJ2 : vitoriasJ1
        );
    }

}
