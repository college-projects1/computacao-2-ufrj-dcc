package src;

public class Main {

    public static void main(String[] args) {
        DadosDeGamao dg = new DadosDeGamao();
        DadosTriplos dt = new DadosTriplos();
        JogoMalucoComSorteadores oJogo = new JogoMalucoComSorteadores(
            "O Jogo", 
            "Isaac Newton", 
            "Leibniz", 
            5, 
            dg, 
            dt
        );

        for(int i=1; i<=100; i++){
            String res = oJogo.jogar();
            System.out.println(res);
        }
    }
}
