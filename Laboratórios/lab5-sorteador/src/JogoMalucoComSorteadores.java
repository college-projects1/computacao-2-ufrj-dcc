package src;

public class JogoMalucoComSorteadores extends JogoDeDoisJogadores {
    Sorteador sorteador1;
    Sorteador sorteador2;

    public JogoMalucoComSorteadores(
        String nomeJogo, 
        String nomeJogador1, 
        String nomeJogador2, 
        int numeroDeRodadas,
        Sorteador sorteador1,
        Sorteador sorteador2
    ){
        super(nomeJogo, nomeJogador1, nomeJogador2, numeroDeRodadas);
        this.sorteador1 = sorteador1;
        this.sorteador2 = sorteador2;
    }

    @Override
    protected String executarRodadaDoJogo(){
        int roll1 = sorteador1.sortear();
        int roll2 = sorteador2.sortear();

        if(roll1 > roll2){
            return nomeJogador1;
        } else if(roll2 > roll1){
            return nomeJogador2;
        } 
        return "empate";
    }
    
}
