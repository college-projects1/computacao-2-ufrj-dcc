package src;

import java.util.Random;

public class Dado implements Sorteador {

    public int sortear() {
        Random dado = new Random();
        int roll = dado.nextInt(6)+1;
        return roll;
    }
}
