package src;

public class DadosTriplos implements Sorteador {

    public int sortear() {
        Dado dado = new Dado();

        int roll1 = dado.sortear();
        int roll2 = dado.sortear();
        int roll3 = dado.sortear();

        return roll1+roll2+roll3;
    }
    
}
