package src;

public class DadosDeGamao implements Sorteador {

    public int sortear() {
        Dado dado = new Dado();

        int roll1 = dado.sortear();
        int roll2 = dado.sortear();

        if(roll1 == roll2) { return 2*(roll1+roll2); }

        return roll1+roll2;
    }
    
}
