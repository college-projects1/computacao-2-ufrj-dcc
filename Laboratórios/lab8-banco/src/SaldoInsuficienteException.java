public class SaldoInsuficienteException extends Exception {
    private static final long serialVersionUID = 1L;
    private float valorAlemDoLimite;

    public SaldoInsuficienteException(float valorAlemDoLimite) {
        this.valorAlemDoLimite = valorAlemDoLimite;
    }

    public float getValorAlemDoLimite() {
        return valorAlemDoLimite;
    }
}
