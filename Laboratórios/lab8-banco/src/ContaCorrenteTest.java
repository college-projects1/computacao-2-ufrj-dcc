import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ContaCorrenteTest {

    private static final float ERRO_ACEITAVEL_NOS_FLOATS = 0.000001f;

    private Pessoa maria;
    private Pessoa joao;

    private Banco banco;

    private Agencia minhaAgencia;

    private ContaCorrente contaDaMaria;
    private ContaCorrente contaDoJoao;

    @Before
    public void setUp() {
        banco = new Banco();

        // cria algumas pessoas
        maria = new Pessoa("Maria", 12345678);
        joao = new Pessoa("Joao", 23222);

        // cria uma agencia
        minhaAgencia = new Agencia(banco, 1, "Agência Principal");

        ContaCorrente.numeroDeContasCriadas = 0;  // reseta o static da classe

        // cria algumas contas
        contaDaMaria = new ContaCorrente(maria, minhaAgencia);
        contaDoJoao = new ContaCorrente(joao, minhaAgencia);
    }

    @Test
    public void testarNumerosAutomaticosDeContas() {
        assertEquals(1, contaDaMaria.getNumeroDaConta());
        assertEquals(2, contaDoJoao.getNumeroDaConta());
//        ContaCorrente novaConta = new ContaCorrente(maria, minhaAgencia);
//        long numeroDaConta = novaConta.getNumeroDaConta();
//
//        assertEquals(numeroDaConta + 1,
//                (new ContaCorrente(joao, minhaAgencia).getNumeroDaConta()));
//        assertEquals(numeroDaConta + 2,
//                (new ContaCorrente(joao, minhaAgencia).getNumeroDaConta());
    }

    @Test
    public void testarDeposito() {
        checarSaldoInicial(contaDaMaria);

        try {
            contaDaMaria.depositar(1000);
        } catch (ValorInvalidoException e) {
            e.printStackTrace();
            fail("Retornou exceção, valor inválido");
        }
        assertFloatEquals(1010f, contaDaMaria.getSaldoEmReais());

        try {
            contaDaMaria.depositar(500);
        } catch (ValorInvalidoException e) {
            e.printStackTrace();
            fail("Retornou exceção, valor inválido");
        }
        assertFloatEquals(1510f, contaDaMaria.getSaldoEmReais());

        try {
            contaDaMaria.depositar(-100);
            fail("Deveria retornar exceção, valor inválido");
        } catch (ValorInvalidoException e) {
            e.printStackTrace();
        }
        assertFloatEquals(1510f, contaDaMaria.getSaldoEmReais());  // nada mudou,porque o depósito não foi feito
   }

    @Test
    public void testarSaque() {
        checarSaldoInicial(contaDaMaria);
        try {
            contaDaMaria.sacar(7);
        } catch (SaqueDeValorNaoPositivoException e) {
            e.printStackTrace();
            fail("Retornou exceção, valor negativo");
        } catch (SaldoInsuficienteException e) {
            e.printStackTrace();
            fail("Retornou exceção, saldo insuficiente");
        }
        assertEquals(3f, contaDaMaria.getSaldoEmReais(), ERRO_ACEITAVEL_NOS_FLOATS);
    }

    @Test
    public void testarSaqueSemFundos() {
        // ALTERADO PARA ACEITAR CHEQUE ESPECIAL
        checarSaldoInicial(contaDaMaria);
        try {
            contaDaMaria.sacar(17);
            assertFloatEquals(-7f, contaDaMaria.getSaldoEmReais());
        } catch (SaqueDeValorNaoPositivoException e) {
            e.printStackTrace();
            fail("Retornou exceção, valor negativo");
        } catch (SaldoInsuficienteException e) {
            e.printStackTrace();
            fail("Retornou exceção, saldo insuficiente");
        }
    }

    @Test
    public void testarTransferecia() {
        // sanity check: as contas já começam com saldo 10 (regra de negócio)
        checarSaldoInicial(contaDaMaria);
        checarSaldoInicial(contaDoJoao);

        try {
            contaDaMaria.transferir(7, contaDoJoao);
        } catch (ValorInvalidoException e) {
            e.printStackTrace();
            fail("Retornou exceção, valor inválido");
        }

        assertFloatEquals(3f, contaDaMaria.getSaldoEmReais());
        assertFloatEquals(17f, contaDoJoao.getSaldoEmReais());
    }

    @Test
    public void testarTransfereciaSemFundosNaContaDeOrigem() {
        // sanity check: as contas já começam com saldo 10 (regra de negócio)
        // ALTERADO PARA ACEITAR CHEQUE ESPECIAL
        assertFloatEquals(10f, contaDaMaria.getSaldoEmReais());
        assertFloatEquals(10f, contaDoJoao.getSaldoEmReais());

        try {
            contaDaMaria.transferir(200, contaDoJoao);
            assertFloatEquals(-190f, contaDaMaria.getSaldoEmReais());
            assertFloatEquals(210f, contaDoJoao.getSaldoEmReais());
        } catch (ValorInvalidoException e) {
            e.printStackTrace();
            fail("Retornou exceção, valor inválido");
        }
    }

    private void checarSaldoInicial(ContaCorrente conta) {
        // sanity check: as contas já começam com saldo 10 (regra de negócio)
        assertFloatEquals(
                ContaCorrente.SALDO_INICIAL_DE_NOVAS_CONTAS,
                conta.getSaldoEmReais());
    }

    private static void assertFloatEquals(float expected, float actual) {
        assertEquals(expected, actual, ERRO_ACEITAVEL_NOS_FLOATS);
    }

    @Test
    public void testarDepositarException() {
        try {
            contaDaMaria.depositar(0);
            fail("Deveria retornar exceção, valor inválido");
        } catch (ValorInvalidoException e) {
            e.printStackTrace();
            assertFloatEquals(10f, contaDaMaria.getSaldoEmReais());
        }
    }

    @Test
    public void testarSacarException() {
        try {
            contaDaMaria.sacar(-10);
            fail("Deveria retornar exceção, valor negativo");
        } catch (SaqueDeValorNaoPositivoException e) {
            e.printStackTrace();
            assertFloatEquals(10f, contaDaMaria.getSaldoEmReais());
        } catch (SaldoInsuficienteException e) {
            e.printStackTrace();
            fail("Retornou a exceção errada, deveria ser valor negativo");
        }

        try {
            contaDaMaria.sacar(1000);
            fail("Deveria retornar exceção, saldo insuficiente");
        } catch (SaldoInsuficienteException e) {
            e.printStackTrace();
            assertFloatEquals(10f, contaDaMaria.getSaldoEmReais());
        }  catch (SaqueDeValorNaoPositivoException e){
            fail("Retornou a exceção errada, deveria ser saldo insuficiente");
        }
    }

    @Test
    public void testarTransferirException() {
        try {
            contaDaMaria.transferir(-10, contaDoJoao);
            fail("Deveria retornar exceção, valor inválido");
        } catch (ValorInvalidoException e) {
            e.printStackTrace();
            assertFloatEquals(10f, contaDaMaria.getSaldoEmReais());
        }

        try {
            contaDaMaria.transferir(1000, contaDoJoao);
            fail("Deveria retornar exceção, saldo insuficiente");
        } catch (ValorInvalidoException e) {
            e.printStackTrace();
            assertFloatEquals(10f, contaDaMaria.getSaldoEmReais());
        }
    }

}