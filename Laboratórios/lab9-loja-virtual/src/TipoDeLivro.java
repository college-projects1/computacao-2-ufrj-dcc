public enum TipoDeLivro {
    ENCICLOPEDIA("ENC"),
    DIDATICO("LD"),
    FICCAO("FIC"),
    BIOGRAFIA("BIO"),
    ARTE("AR"),
    DICIONARIO("DIC"),
    NAOFICCAO("NF");

    private String abreviacao;
    
    TipoDeLivro(String abreviacao) {
        this.abreviacao = abreviacao;
    }

    public String getAbreviacao() {
        return abreviacao;
    }
}
