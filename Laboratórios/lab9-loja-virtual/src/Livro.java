import java.util.Objects;

public class Livro extends ArtigoCultural {

    private final int codigoISBN;

    private final TipoDeLivro tipo;

    private String titulo;

    private String autor;

    private String editora;

    private int anoPublicacao;

    private int numeroDePaginas;

    public Livro(int codigoISBN, TipoDeLivro tipo,
                 String titulo, String autor, String editora, int anoPublicacao) {

        super(codigoISBN,
                String.format("Livro: %s (%s, %d)",
                titulo, autor, anoPublicacao));

        this.codigoISBN = codigoISBN;
        this.tipo = tipo;
        this.titulo = titulo;
        this.autor = autor;
        this.editora = editora;
        this.anoPublicacao = anoPublicacao;
    }

    public int getCodigoISBN() {
        return codigoISBN;
    }

    public TipoDeLivro getTipo() {
        return this.tipo;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getEditora() {
        return editora;
    }

    public void setEditora(String editora) {
        this.editora = editora;
    }

    public int getAnoPublicacao() {
        return anoPublicacao;
    }

    public void setAnoPublicacao(int anoPublicacao) {
        this.anoPublicacao = anoPublicacao;
    }

    public int getNumeroDePaginas() {
        return numeroDePaginas;
    }

    public void setNumeroDePaginas(int numeroDePaginas) {
        this.numeroDePaginas = numeroDePaginas;
    }

    @Override
    public String toString() {
        return  "codigoISBN: " + codigoISBN +
                "Categoria: " + tipo.getAbreviacao() +
                "Titulo: " + titulo +
                "Autor" + autor +
                "Editora:" + editora +
                "Anode publicação" + anoPublicacao;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Livro livro = (Livro) o;
        return codigoISBN == livro.codigoISBN &&
            tipo == livro.tipo &&
            Objects.equals(titulo, livro.titulo) && 
            Objects.equals(autor, livro.autor) && 
            Objects.equals(editora, livro.editora) && 
            Objects.equals(anoPublicacao, livro.anoPublicacao) && 
            Objects.equals(numeroDePaginas, livro.numeroDePaginas);
    }

    @Override
    public int hashCode() {
        return Objects.hash(codigoISBN, tipo);
    }
}
