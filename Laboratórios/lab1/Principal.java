import java.util.Scanner; 
import java.util.Locale;

public class Principal {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);  
        scanner.useLocale(Locale.US);

        int numberOfStudents = 0;

        float studentMean = 0.0f;
        float biggestMean = 0.0f;
        
        float classRoomMean = 0f;
        float classRoomSum = 0f;

        long biggestMeanStudent = 0L;
        long student = 0L;
       
        System.out.println("Insira o DRE acompanhadoo da média do aluno (118064962 10.0)");
        student = scanner.nextLong();
        studentMean = scanner.nextFloat();

        while(studentMean >= 0) {
            numberOfStudents++;
            classRoomSum += studentMean;

            if (biggestMean < studentMean) {
                biggestMean = studentMean;
                biggestMeanStudent = student;
            }
            
            student = scanner.nextInt();
            studentMean = scanner.nextFloat();
        }
        scanner.close();

        //numberOfStudents = students.size();
        classRoomMean = classRoomSum / numberOfStudents;

        System.out.println("Quantidade de notas: " + numberOfStudents);
        System.out.println("Média da turma: " + classRoomMean);
        System.out.println("Aluno de maior média: " + biggestMeanStudent + " ("+ biggestMean +")");
    }
}