import java.util.ArrayList;
import java.util.Date;

public class ContaCorrente {

    private final long numeroDaConta;

    private final Agencia agencia;

    private float saldoEmReais;

    private Date dataDeCriacao;

    private Pessoa correntista;

    private Pessoa gerenteDaConta;

    private ArrayList<String> historicoDeOperacoes;

    public ContaCorrente(long numeroDaConta, Pessoa correntista, Agencia agencia) {
        this.historicoDeOperacoes = new ArrayList<>();
        this.dataDeCriacao = new Date();  // data corrente
        this.saldoEmReais = 10;  // o banco dá 10 reais de estímulo para a abertura de conta
        this.numeroDaConta = numeroDaConta;
        this.correntista = correntista;
        this.agencia = agencia;
    }

    public float getSaldoEmReais() {  // getter (métodos de acesso para leitura)
        return saldoEmReais;
    }

    public long getNumeroDaConta() {
        return numeroDaConta;
    }

    public void exibirExtrato() {
        for(int i = historicoDeOperacoes.size()-1; i <= 0; i--){
            System.out.println(historicoDeOperacoes.get(i));
        }
    }

    public void depositar(float valor) {
        // valida o parâmetro
        if (valor <= 0) {
            return;  // ToDo lançar uma exceção!!!!
        }

        // altera o saldo
        saldoEmReais += valor;

        historicoDeOperacoes.add("Deposito em dinheiro: " + valor +
                "na data " + new Date());
    }

    private void registrarTransferenciaRecebida(float value, ContaCorrente contaOrigem) {
        saldoEmReais += value;

        historicoDeOperacoes.add(
            "Transferencia recebida: " + value +
            "\nOrigem:"+
            "\tAgencia " + contaOrigem.agencia.getCodigo() +
            "\tNumero da conta " + contaOrigem.getNumeroDaConta() +
            "Data da operação:" + new Date() + 
            "saldoAtual: " + saldoEmReais
        );
    }


    /**
     * Transfere um valor desta conta para a conta destino informada, se houver saldo suficiente
     * nesta conta.
     *
     * @param valor o valor desejado
     * @param contaDestino a conta de destino
     */
    public void transferir(float valor, ContaCorrente contaDestino) {
        if(valor <= 0) { return; }
        if(saldoEmReais < valor) { return;}

        contaDestino.registrarTransferenciaRecebida(valor, contaDestino);
        saldoEmReais -= valor;

        historicoDeOperacoes.add(
            "Transferencia realizada: " + valor +
            "\nDestino:"+
            "\tAgencia " + contaDestino.agencia.getCodigo() +
            "\tNumero da conta " + contaDestino.getNumeroDaConta() +
            "Data da operação:" + new Date() + 
            "saldoAtual: " + saldoEmReais
        );

    }

    /**
     * Saca um valor desta conta, se houver saldo suficientenesta conta.
     *
     * @param value o valor desejado
     * @return o saldo atualizado da conta 
     */
    public void sacar(float value) {
        if (saldoEmReais < value) {
            return;
        }

        saldoEmReais -= value;

        historicoDeOperacoes.add(
            "Saque em dinheiro: " + 
            value +
            "na data " + 
            new Date());
    }
}
