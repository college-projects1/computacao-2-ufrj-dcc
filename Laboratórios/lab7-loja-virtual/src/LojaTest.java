import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class LojaTest {

    Loja americana;
    Livro livro;
    CD cd;
    Usuario comprador;
    Transportadora gatoPreto;
    Impressora impressoraJatoDeTinta;
    Bicicleta bicicleta;

    @Before
    public void setUp() {
        gatoPreto = new Transportadora();
        impressoraJatoDeTinta = new ImpressoraJatoDeTinta("HP", 2018);
        americana = new Loja(gatoPreto, impressoraJatoDeTinta); 

        comprador = new Usuario(111111, "Rejane");
        comprador.setEndereco("flor de liz, 7");

        livro = new Livro(12345, "Da Terra à Lua", "Julio Verne", null, 1865);
        livro.setPrecoEmReais(25);

        cd = new CD(121223, "The Number of de Beast", "Iron Maiden", 1985);
        cd.setPrecoEmReais(18.50f);

        bicicleta = new Bicicleta(9999, 700, "Monark");
        bicicleta.setPrecoEmReais(580);
        
        americana.incluirItem(livro);
        americana.incluirItem(cd);
        americana.incluirItem(bicicleta);
        americana.atualizarQuantidadeEmEstoquePorItem(livro, 100);
        americana.atualizarQuantidadeEmEstoquePorItem(cd, 50);
        americana.atualizarQuantidadeEmEstoquePorItem(bicicleta, 15);
    }

    @Test
    public void testarVendaParaProdutoExistente() {
        String recibo = americana.receberPedido(livro, 5, comprador);
        assertNotNull(recibo);
        System.out.println(recibo);

        recibo = americana.receberPedido(cd, 1, comprador);
        assertNotNull(recibo);
        System.out.println(recibo);

        recibo = americana.receberPedido(bicicleta, 3, comprador);
        assertNotNull(recibo);
        System.out.println(recibo);
    }

    @Test
    public void testarVendaParaProdutoNaoExistente() {
        Livro livroNaoExistente = new Livro(1010101, "Blah", "Qualquer coisa", null, 2000);
        
        try {
            americana.receberPedido(livroNaoExistente, 5, comprador);;
            fail("Deveria haver uma exceção");
        } catch (RuntimeException e) {
            assertEquals("O item existe no catálogo da loja", e.getMessage());
        }
    }
}