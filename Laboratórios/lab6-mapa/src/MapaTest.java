import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;
import java.util.HashMap;

import java.util.Random;

import static org.junit.Assert.*;

public class MapaTest {

    private Random random = new Random();

    private Map<Long, String> mapaUsandoDoisArraysParalelos;
    private Map<Long, String> mapaUsandoArrayUnico;
    private Map<Long, String> mapaUsandoArrayOrdenado;
    private Map<Long, String> hashMap;

    @Before
    public void setUp() {
        mapaUsandoDoisArraysParalelos = new MapaUsandoDoisArraysParalelos<>();
        mapaUsandoArrayUnico = new MapaUsandoArrayUnico<>();
        mapaUsandoArrayOrdenado = new MapaUsandoArrayOrdenado<>();
        hashMap = new HashMap<>();
        hashMap.put(0L, "value");
    }

    @Test
    public void testeFuncionalidadeBasica() {
        rodarTesteDaFuncionalidadeBasica(mapaUsandoArrayUnico);
        rodarTesteDaFuncionalidadeBasica(mapaUsandoArrayOrdenado);
        rodarTesteDaFuncionalidadeBasica(mapaUsandoDoisArraysParalelos);
        rodarTesteDaFuncionalidadeBasica(hashMap);
    }

    private void rodarTesteDaFuncionalidadeBasica(Map<Long, String> mapa) {
        mapa.put(1234L, "Qualquer Coisa");
        mapa.put(2222L, "Outra Coisa Qualquer");

        assertEquals("Outra Coisa Qualquer", mapa.get(2222L));
        assertEquals("Qualquer Coisa", mapa.get(1234L));
        assertNull(mapa.get(8798798L));
    }

    @Test
    public void testeAtualizacaoParaChaveExistente() {
        rodarTesteAtualizacaoParaChaveExistente(mapaUsandoArrayUnico);
        rodarTesteAtualizacaoParaChaveExistente(mapaUsandoArrayOrdenado);
        rodarTesteAtualizacaoParaChaveExistente(mapaUsandoDoisArraysParalelos);
        rodarTesteAtualizacaoParaChaveExistente(hashMap);
    }

    private void rodarTesteAtualizacaoParaChaveExistente(Map<Long, String> mapa) {
        mapa.put(1234L, "Qualquer Coisa");
        mapa.put(1234L, "Qualquer Coisa Modificada");

        assertEquals("Qualquer Coisa Modificada", mapa.get(1234L));
    }

    @Test
    public void testarPerformance() {
        rodarTesteDePerformance(mapaUsandoArrayUnico);
        rodarTesteDePerformance(mapaUsandoArrayOrdenado);
        rodarTesteDePerformance(mapaUsandoDoisArraysParalelos);
        rodarTesteDePerformance(hashMap);
    }

    private void rodarTesteDePerformance(Map<Long, String> mapa) {
        System.out.println("\nRodando teste de performance para a classe " +
                mapa.getClass().getName() + "...");

        final int TAMANHO = 40_000;

        System.out.println("Vou fazer as inserções...");

        long inicio = System.currentTimeMillis();
        for (long i = 0; i < TAMANHO; i++) {
            long x = random.nextInt(1_000_000);
            mapa.put(x, String.format("%d^2 = %d", x, x*x));
        }
        long duracao = System.currentTimeMillis() - inicio;
        System.out.printf("tamanho = %d --- duracao = %.3f segundos\n",
                TAMANHO, duracao / 1000f);

        System.out.println("Vou fazer as buscas...");
        inicio = System.currentTimeMillis();
        for (long i = 0; i < TAMANHO; i++) {
            mapa.get(random.nextLong());
        }
        duracao = System.currentTimeMillis() - inicio;
        System.out.printf("tamanho = %d --- duracao = %.3f segundos\n",
                TAMANHO, duracao / 1000f);
    }

    @Test
    public void testSizeArrayOrdenado() {
        mapaUsandoArrayOrdenado.put(0L, "valor");
        assertEquals(1, mapaUsandoArrayOrdenado.size());
    }

    @Test
    public void testIsEmptyArrayOrdenado() {
        mapaUsandoArrayOrdenado.put(0L, "valor");
        assertEquals(false, mapaUsandoArrayOrdenado.isEmpty());
    }

    @Test
    public void testContainsKeyArrayOrdenado() {
        mapaUsandoArrayOrdenado.put(0L, "valor");
        assertEquals(true, mapaUsandoArrayOrdenado.containsKey(0L));
    }

    @Test
    public void testContainsValueArrayOrdenado() {
        mapaUsandoArrayOrdenado.put(0L, "value");
        assertEquals(true, mapaUsandoArrayOrdenado.containsValue("value"));
    }

    @Test
    public void testRemoveArrayOrdenado() {
        mapaUsandoArrayOrdenado.put(0L, "value");
        mapaUsandoArrayOrdenado.remove(0L);
        assertEquals(false, mapaUsandoArrayOrdenado.containsKey(0L));
    }

    @Test
    public void testPutAllArrayOrdenado() {
        mapaUsandoArrayOrdenado.putAll(hashMap);
        assertEquals("value", mapaUsandoArrayOrdenado.get(0L));
    }

    @Test
    public void testClearArrayOrdenado() {
        mapaUsandoArrayOrdenado.put(0L, "value");
        mapaUsandoArrayOrdenado.clear();
        assertEquals(false, mapaUsandoArrayOrdenado.containsKey(0L));
        assertEquals(0, mapaUsandoArrayOrdenado.size());
    }

    @Test
    public void testKeySetArrayOrdenado() {
        mapaUsandoArrayOrdenado.put(0L, "value");
        mapaUsandoArrayOrdenado.put(1L, "value");

        Set<Long> kset = mapaUsandoArrayOrdenado.keySet();
        
        assertEquals(true, kset.contains(0L));
        assertEquals(true, kset.contains(1L));
    }

    @Test
    public void testValuesArrayOrdenado() {
        mapaUsandoArrayOrdenado.put(0L, "value");
        mapaUsandoArrayOrdenado.put(1L, "valor");

        Collection<String> kset = mapaUsandoArrayOrdenado.values();
        
        assertEquals(true, kset.contains("value"));
        assertEquals(true, kset.contains("valor"));
    }

    @Test
    public void testEntrySetArrayOrdenado() {
        mapaUsandoArrayOrdenado.put(0L, "value");

        Set<Entry<Long, String>> set = mapaUsandoArrayOrdenado.entrySet();
        ArrayList<Entry<Long, String>> entryList = new ArrayList<>(set);
        
        assertEquals("value", entryList.get(0).getValue());
    }

    @Test
    public void testSizeArrayUnico() {
        mapaUsandoArrayUnico.put(0L, "valor");
        assertEquals(1, mapaUsandoArrayUnico.size());
    }


    @Test
    public void testIsEmptyArrayUnico() {
        mapaUsandoArrayUnico.put(0L, "valor");
        assertEquals(false, mapaUsandoArrayUnico.isEmpty());
    }

    @Test
    public void testContainsKeyArrayUnico() {
        mapaUsandoArrayUnico.put(0L, "valor");
        assertEquals(true, mapaUsandoArrayUnico.containsKey(0L));
    }

    @Test
    public void testContainsValueArrayUnico() {
        mapaUsandoArrayUnico.put(0L, "value");
        assertEquals(true, mapaUsandoArrayUnico.containsValue("value"));
    }

    @Test
    public void testRemoveArrayUnico() {
        mapaUsandoArrayUnico.put(0L, "value");
        mapaUsandoArrayUnico.remove(0L);
        assertEquals(false, mapaUsandoArrayUnico.containsKey(0L));
    }

    @Test
    public void testPutAllArrayUnico() {
        mapaUsandoArrayUnico.putAll(hashMap);
        assertEquals("value", mapaUsandoArrayUnico.get(0L));
    }

    @Test
    public void testClearArrayUnico() {
        mapaUsandoArrayUnico.put(0L, "value");
        mapaUsandoArrayUnico.clear();
        assertEquals(false, mapaUsandoArrayUnico.containsKey(0L));
        assertEquals(0, mapaUsandoArrayUnico.size());
    }

    @Test
    public void testKeySetArrayUnico() {
        mapaUsandoArrayUnico.put(0L, "value");
        mapaUsandoArrayUnico.put(1L, "value");

        Set<Long> kset = mapaUsandoArrayUnico.keySet();
        
        assertEquals(true, kset.contains(0L));
        assertEquals(true, kset.contains(1L));
    }

    @Test
    public void testValuesArrayUnico() {
        mapaUsandoArrayUnico.put(0L, "value");
        mapaUsandoArrayUnico.put(1L, "valor");

        Collection<String> kset = mapaUsandoArrayUnico.values();
        
        assertEquals(true, kset.contains("value"));
        assertEquals(true, kset.contains("valor"));
    }

    @Test
    public void testEntrySetArrayUnico() {
        mapaUsandoArrayUnico.put(0L, "value");

        Set<Entry<Long, String>> set = mapaUsandoArrayUnico.entrySet();
        ArrayList<Entry<Long, String>> entryList = new ArrayList<>(set);
        
        assertEquals("value", entryList.get(0).getValue());
    }

}