import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.HashSet;

public class MapaUsandoArrayOrdenado<C extends Comparable<C>, V> implements Map<C, V> {

    private ArrayList<ParChaveValor<C, V>> minhaListaOrdenadaDePares;

    public MapaUsandoArrayOrdenado() {
        this.minhaListaOrdenadaDePares = new ArrayList<>();
    }

    @Override
    public int size() {
        return this.minhaListaOrdenadaDePares.size();
    }

    @Override
    public boolean isEmpty() {
        return this.minhaListaOrdenadaDePares.size() == 0;
    }

    @Override
    public boolean containsKey(Object key) {
        for(ParChaveValor<C, V> p : minhaListaOrdenadaDePares){
            if(p.getKey().equals(key)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean containsValue(Object value) {
        for(ParChaveValor<C, V> p : minhaListaOrdenadaDePares){
            if(p.getValue().equals(value)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public V put(C chave, V valor) {

        int posicaoParaInsercao = -1;

        for (int i = 0; i < this.minhaListaOrdenadaDePares.size(); i++) {
            ParChaveValor<C, V> par = this.minhaListaOrdenadaDePares.get(i);
            if (par.getKey().equals(chave)) {
                par.setValue(valor);
                return par.getValue();
            }

            if (par.getKey().compareTo(chave) > 0) {
                // a chave da posição que estou olhando é maior do que a chave que quero put

                posicaoParaInsercao = i;
                break;  // saio do for, pois já encontrei a posição para inserir
            }
        }

        if (posicaoParaInsercao == -1) {
            // minha chave é maior do que todas que existiam na lista,
            // então quero adicioná-la de fato no final da lista ordenada
            this.minhaListaOrdenadaDePares.add(new ParChaveValor<>(chave, valor));

        } else {
            // preciso inserir "no meio" da lista ordenada, então
            // antes vou mover os pares pré-existentes uma casa para a direita
            final ParChaveValor<C, V> ultimoParDaLista =
                    this.minhaListaOrdenadaDePares.get(getTamanho() - 1);

            this.minhaListaOrdenadaDePares.add(ultimoParDaLista);
            // isso abrirá uma nova posição no fim da lista,
            // repetindo a referência àquele mesmo último objeto ParChaveValor

            for (int i = getTamanho() - 3; // a penúltimo posição da lista ANTES do add
                 i >= posicaoParaInsercao;
                 i--) {

                // shift right
                this.minhaListaOrdenadaDePares.set(i + 1,
                        this.minhaListaOrdenadaDePares.get(i));
            }

            // agora sim posso setar o meu elemento na posição desejada
            this.minhaListaOrdenadaDePares.set(posicaoParaInsercao,
                    new ParChaveValor<>(chave, valor));
        }

        return valor;
    }

    @Override
    public V remove(Object key) {
        for (ParChaveValor<C, V> par : this.minhaListaOrdenadaDePares) {
            V pValor = par.getValue();
            if (par.getKey().equals(key)) {
                minhaListaOrdenadaDePares.remove(par);
                return pValor;
            }
        }
        return null;
    }

    @Override
    public void putAll(Map<? extends C, ? extends V> m) {
        for (Map.Entry<? extends C, ? extends V> e : m.entrySet()) {
            C key = e.getKey();
            V value = e.getValue();
            put(key, value);
        }
    }

    @Override
    public void clear() {
        this.minhaListaOrdenadaDePares.clear();
    }

    @Override
    public Set<C> keySet() {
        Set<C> set = new HashSet<>();
        for(ParChaveValor<C, V> par : minhaListaOrdenadaDePares) {
            set.add(par.getKey());
        }
        return set;
    }

    @Override
    public Collection<V> values() {
        Collection<V> collection = new HashSet<>(); 
        for(ParChaveValor<C, V> par : minhaListaOrdenadaDePares) {
            collection.add(par.getValue());
        }
        return collection;
    }

    @Override
    public Set<Entry<C, V>> entrySet() {
        Set<Entry<C, V>> set = new HashSet<>();
        for(ParChaveValor<C, V> par : minhaListaOrdenadaDePares) {
            set.add(par);
        }
        return set;
    }

    @Override
    public V get(Object chaveDeBusca) {
        ParChaveValor<C, V> par = obterParChaveValor(chaveDeBusca);
        return par == null
                ? null
                : par.getValue();
    }

    @SuppressWarnings("unchecked")
    private ParChaveValor<C, V> obterParChaveValor(Object chave) {
        for (ParChaveValor<C, V> par : this.minhaListaOrdenadaDePares) {
            if (par.getKey().equals(chave)) {
                return par;
            } else if (par.getKey().compareTo((C) chave) > 0) {
                break;
            }
        }
        return null;
    }

    public int getTamanho() {
        return this.minhaListaOrdenadaDePares.size();
    }

    public String toString() {
        String s = "{";
        for(ParChaveValor<C, V> par : this.minhaListaOrdenadaDePares) {
            s += par.getKey()+"="+par.getValue()+",";
        }
        return s+="}";
    }
}
