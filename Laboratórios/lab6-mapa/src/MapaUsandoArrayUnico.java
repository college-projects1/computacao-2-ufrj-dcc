import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.HashSet;

public class MapaUsandoArrayUnico<C, V> implements Map<C, V> {

    private ArrayList<ParChaveValor<C, V>> minhaListaDePares;

    public MapaUsandoArrayUnico() {
        this.minhaListaDePares = new ArrayList<>();
    }

    @Override
    public int size() {
        return this.minhaListaDePares.size();
    }

    @Override
    public boolean isEmpty() {
        return this.minhaListaDePares.size() == 0;
    }

    @Override
    public boolean containsKey(Object key) {
        for(ParChaveValor<C, V> p : minhaListaDePares){
            if(p.getKey().equals(key)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean containsValue(Object value) {
        for(ParChaveValor<C, V> p : minhaListaDePares){
            if(p.getValue().equals(value)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public V put(C chave, V valor) {
        ParChaveValor<C, V> parPreExistente = obterParChaveValor(chave);

        if (parPreExistente == null) {  // chave inédita!!
            this.minhaListaDePares.add(new ParChaveValor<>(chave, valor));
            return valor;

        }
        
        V valorPreExistente = parPreExistente.getValue();
        parPreExistente.setValue(valor);

        return valorPreExistente;
    }

    @Override
    public V remove(Object key) {
        for (ParChaveValor<C, V> par : this.minhaListaDePares) {
            V pValor = par.getValue();
            if (par.getKey().equals(key)) {
                minhaListaDePares.remove(par);
                return pValor;
            }
        }
        return null;
    }

    @Override
    public void putAll(Map<? extends C, ? extends V> m) {
        for (Map.Entry<? extends C, ? extends V> e : m.entrySet()) {
            C key = e.getKey();
            V value = e.getValue();
            put(key, value);
        }
    }

    @Override
    public void clear() {
        this.minhaListaDePares.clear();
    }

    @Override
    public Set<C> keySet() {
        Set<C> set = new HashSet<>();
        for(ParChaveValor<C, V> par : minhaListaDePares) {
            set.add(par.getKey());
        }
        return set;
    }

    @Override
    public Collection<V> values() {
        Collection<V> collection = new HashSet<>(); 
        for(ParChaveValor<C, V> par : minhaListaDePares) {
            collection.add(par.getValue());
        }
        return collection;
    }

    @Override
    public Set<Entry<C, V>> entrySet() {
        Set<Entry<C, V>> set = new HashSet<>();
        for(ParChaveValor<C, V> par : minhaListaDePares) {
            set.add(par);
        }
        return set;
    }

    @Override
    public V get(Object chaveDeBusca) {
        ParChaveValor<C, V> par = obterParChaveValor(chaveDeBusca);
        return par == null
                ? null
                : par.getValue();
    }

    private ParChaveValor<C, V> obterParChaveValor(Object chave) {
        for (ParChaveValor<C, V> par : this.minhaListaDePares) {
            if (par.getKey().equals(chave)) {
                return par;
            }
        }
        return null;
    }
}
