import java.util.Map.Entry;

public class ParChaveValor<C, V> implements Entry<C, V> {

    private C chave;
    private V valor;

    public ParChaveValor(C chave, V valor) {
        this.chave = chave;
        this.valor = valor;
    }

    public void setKey(C chave) {
        this.chave = chave;
    }

    @Override
    public C getKey() {
        return chave;
    }

    @Override
    public V getValue() {
        return valor;
    }

    @Override
    public V setValue(V valor) {
        this.valor = valor;
        return valor;
    }

    @Override
    public String toString() {
        return "ParChaveValor{" +
                "chave=" + chave +
                ", valor=" + valor +
                '}';
    }
}
