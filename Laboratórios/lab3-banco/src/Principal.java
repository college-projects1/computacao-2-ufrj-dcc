import java.util.Scanner;
import java.util.HashMap;
import java.util.Map;

public class Principal {
    public static void main(String[] args) {
        String option = "";
        Pessoa client;

        Agencia ag = new Agencia();

        Scanner sc = new Scanner(System.in);

        Map<Integer, ContaCorrente> accounts = new HashMap<Integer, ContaCorrente>();
        Map<Long, Pessoa> clients = new HashMap<Long, Pessoa>();

        do {
            System.out.println("(D)epositar");
            System.out.println("(S)acar");
            System.out.println("(T)ransferir");
            System.out.println("(C)onsultar saldo");
            System.out.println("Cadastrar (P)essoa como correntista");
            System.out.println("Criar (N)ova conta");
            System.out.println("(X) para sair\n");    
            System.out.println("Opção desejada:");

            option = sc.next().toUpperCase();

            if(option.equals("D")) {
                int accountId, value;

                System.out.println("\n$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
                System.out.println("$$$$$$$$$$$$$$$$$$$$$$ DEPOSITO $$$$$$$$$$$$$$$$$$$$$$");
                System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
                System.out.println("\nDigite o numero da conta:");

                accountId = sc.nextInt();
                if(accounts.get(accountId)==null){
                    System.out.println("Conta não registrada");
                    continue;
                }

                System.out.println("Digite o valor em reais que deseja depositar:");

                value = sc.nextInt();
                if (value <= 0) {
                    System.out.println("valor invalido!");
                    continue;
                }

                accounts.get(accountId).depositar(value);
                System.out.println("\nDeposito realizado com sucesso. O saldo atual da conta é de R$"+accounts.get(accountId).getSaldoEmReais()+"0");
                System.out.println("\t-> "+ accounts.get(accountId).getUltimaOperacao()+"\n");

            }

            else if(option.equals("S")) {
                int accountId, value;

                System.out.println("\n$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
                System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$ SAQUE $$$$$$$$$$$$$$$$$$$$$$$");
                System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
                System.out.println("\nDigite o numero da conta:");

                accountId = sc.nextInt();
                if(accounts.get(accountId)==null){
                    System.out.println("Conta não registrada");
                    continue;
                }

                System.out.println("Digite o valor em reais que deseja sacar:");

                value = sc.nextInt();
                if (value <= 0) {
                    System.out.println("valor invalido!");
                    continue;
                }

                accounts.get(accountId).sacar(value);
                System.out.println("\nSaque de R$"+value+" realizado com sucesso. O saldo atual da conta é de R$"+accounts.get(accountId).getSaldoEmReais());
                System.out.println("\t-> "+ accounts.get(accountId).getUltimaOperacao()+"\n");
            }

            else if(option.equals("T")) {
                int accountIdOrigin, accountIdDestination, value;

                System.out.println("\n$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
                System.out.println("$$$$$$$$$$$$$$$$$$$ TRANSFERENCIA $$$$$$$$$$$$$$$$$$$$");
                System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
                System.out.println("\nDigite o numero da conta de origem:");

                accountIdOrigin = sc.nextInt();
                if(accounts.get(accountIdOrigin)==null){
                    System.out.println("Conta origem não registrada");
                    continue;
                }

                System.out.println("\nDigite o numero da conta de destino:");
                accountIdDestination = sc.nextInt();
                if(accounts.get(accountIdDestination)==null){
                    System.out.println("Conta destino não registrada");
                    continue;
                }

                System.out.println("Digite o valor em reais que deseja transferir:");

                value = sc.nextInt();
                if (value <= 0) {
                    System.out.println("valor invalido!");
                    continue;
                }

                accounts.get(accountIdOrigin).transferir(value, accounts.get(accountIdDestination));
                System.out.println("\nTransferencia de R$"+value+" realizado com sucesso.");
                System.out.println("\t-> "+accountIdOrigin+":"+ accounts.get(accountIdOrigin).getUltimaOperacao());
                System.out.println("\t-> "+accountIdDestination+":"+ accounts.get(accountIdDestination).getUltimaOperacao()+"\n");
            }

            else if(option.equals("C")) {
                int accountId;

                System.out.println("\n$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
                System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$ SALDO $$$$$$$$$$$$$$$$$$$$$$$");
                System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
                System.out.println("\nDigite o numero da conta:");
                accountId = sc.nextInt();

                if(accounts.get(accountId)==null){
                    System.out.println("Conta não registrada");
                    continue;
                }

                System.out.println("\nO saldo da conta é de R$"+accounts.get(accountId).getSaldoEmReais()+"0\n");
            }

            else if(option.equals("P")) {
                long CPF;
                String firstName;
                String lastName;

                System.out.println("\nREGISTRANDO CORRENTISTA\n");
                System.out.println("Digite o CPF do cliente:");

                CPF = sc.nextLong();
                
                if(clients.get(CPF) != null){
                    System.out.println("Cliente já registrado.");
                    continue;
                }

                System.out.println("Digite o primeiro e ultimo nome do cliente:");
                firstName = sc.next();
                lastName = sc.next();

                client = new Pessoa(firstName+" "+lastName, CPF);
                clients.put(CPF, client);

                System.out.println("\n--- Cliente registrado com sucesso ---\n");
            }

            else if(option.equals("N")) {
                long CPF;
                ContaCorrente account;

                System.out.println("\nREGISTRANDO CONTA\n");
                System.out.println("Digite o CPF do cliente:");

                CPF = sc.nextLong();

                if(clients.get(CPF) == null){
                    System.out.println("Cliente não registrado.");
                    continue;
                }

                account = new ContaCorrente(clients.get(CPF), ag);
                accounts.put(ag.getUltimaContaRegistrada(), account);

                System.out.println("\n--- Conta registrado com sucesso (Numero da conta "+ag.getUltimaContaRegistrada()+" ) ---\n");
            }

            else if(!option.equals("X")) {
                System.out.println("\n!!! --- ERRO: COMANDO INVALIDO --- !!!\n");
            }

        } while(!option.equals("X"));

        System.out.println("\nPrograma encerrado.");
        
        sc.close();
    }
}
