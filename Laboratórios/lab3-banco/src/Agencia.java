public class Agencia {

    private String nome;

    private String endereco;

    private Pessoa gerenteGeral;

    private int codigo;

    private int numeroDeContas;

    public int getUltimaContaRegistrada() {
        return numeroDeContas;
    }

    public int registroDeNovaConta() {
        numeroDeContas++;
        return numeroDeContas;
    }
}
