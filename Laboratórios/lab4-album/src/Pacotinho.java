import java.util.ArrayList;
import java.util.Random;


@SuppressWarnings("serial")
public class Pacotinho extends ArrayList<Figurinha> {

    private Album album;

    public Pacotinho(Album album) {
        this.album = album;
        adicionarFigurinhasAleatorias();

    }

    // sobrecarga no costrutor, passando aqui as posições desejadas
    public Pacotinho(Album album, int[] posicoes) {
        this.album = album;
        
        if(posicoes.length != album.getQuantFigurinhasPorPacotinho()) {
            throw new RuntimeException("Pacotinho no tamanho errado!");
        }

        Figurinha figurinha;
        for(int p : posicoes){
            figurinha = new Figurinha(p);
            this.add(figurinha);
        }

    }

    private void adicionarFigurinhasAleatorias() {
        int maxPosicao = album.getTamanho();
        int quantFigurinhasPorPacotinho = album.getQuantFigurinhasPorPacotinho();

        Figurinha figurinha;

        Random rand = new Random(); 
        
        for (int i = 1; i <= quantFigurinhasPorPacotinho; i++) {
            int posicao = rand.nextInt(maxPosicao) + 1;
            figurinha = new Figurinha(posicao);
            this.add(figurinha);
        }
    }

    public Album getAlbum() {
        return this.album;
    }
}
