public class Figurinha {

    private int posicao;
    private String imagem;

    public Figurinha(int posicao) {
        this.posicao = posicao;
        this.imagem = null;
        
    }


    public Figurinha(int posicao, String imagem) {
        this.posicao = posicao;
        this.imagem = imagem;
    }

    /**
     * @return A posição que esta figurinha deve ocupar no álbum
     */
    public int getPosicao() {
        return this.posicao;
     }

     /**
      * @return A imagem que esta figurinha possui
      */
     public String getImagem() {
        return this.imagem;
     }

    @Override
    public String toString() {
        return "{position: "+this.getPosicao()+", image: "+this.imagem+"}";
    }
}
