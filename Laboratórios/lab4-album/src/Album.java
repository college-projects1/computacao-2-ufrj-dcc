import java.util.ArrayList;

public class Album {

    public static final int PERCENTUAL_MINIMO_PARA_AUTO_COMPLETAR = 90;  // 90%

    private int quantFigurinhasPorPacotinho;
    private int tamanhoDoAlbum;
    private int pacotinhosRecebidos;
    private int quantFigurinhasColadas;

    private Figurinha[] figurinhasNoAlbum; 
    private ArrayList<Figurinha> figurinhasExtras; 

    public Album(int tamanhoDoAlbum, int quantFigurinhasPorPacotinho) {
        this.tamanhoDoAlbum = tamanhoDoAlbum;
        this.quantFigurinhasPorPacotinho = quantFigurinhasPorPacotinho;
        this.figurinhasNoAlbum = new Figurinha[tamanhoDoAlbum];
        this.figurinhasExtras = new ArrayList<Figurinha>();

        this.pacotinhosRecebidos = 0;
        this.quantFigurinhasColadas = 0;


    }

    public void receberNovoPacotinho(Pacotinho pacotinho) {
        for (Figurinha fig : pacotinho) {
            if(this.figurinhasNoAlbum[fig.getPosicao()-1] == null){
                this.figurinhasNoAlbum[fig.getPosicao()-1] = fig;
                this.quantFigurinhasColadas++;
            } else {
                figurinhasExtras.add(fig);
            }
        }
        this.pacotinhosRecebidos++;
    }

    public void autoCompletar() {
        float preenchido = (float) this.getQuantFigurinhasColadas()/this.getTamanho();
        System.out.println("P"+preenchido);
        if (
            100*preenchido >= PERCENTUAL_MINIMO_PARA_AUTO_COMPLETAR &&
            100*preenchido < 100
        ) {
            for(int p=0; p < tamanhoDoAlbum; p++) {
                if (figurinhasNoAlbum[p]==null){
                    figurinhasNoAlbum[p] = new Figurinha(p+1);
                    quantFigurinhasColadas++;    
                }
            }
        }
    }

    public int getTamanho() {
        return this.tamanhoDoAlbum;
    }

    public int getQuantFigurinhasPorPacotinho() {
        return this.quantFigurinhasPorPacotinho;
    }

    public int getQuantFigurinhasColadas() {
        return this.quantFigurinhasColadas;
    }

    public int getQuantFigurinhasRepetidas() {
        return this.figurinhasExtras.size();
    }

    public int getQuantFigurinhasFaltantes(){
        return this.tamanhoDoAlbum-this.quantFigurinhasColadas;
    }

    public int getQuantPacotesRecebidos(){
        return this.pacotinhosRecebidos;
    }

    public boolean possuiFigurinhaColada(int posicao) {
        if(posicao <= 0) { return false; }
        if(posicao > tamanhoDoAlbum) { return false; }
        if(this.figurinhasNoAlbum[posicao-1] != null) { return true; }
        return false;
    }

    public boolean possuiFigurinhaRepetida(int posicao) {
        // O usuário pode ter uma figurinha repetido mais de uma vez
        for (Figurinha fig : figurinhasExtras) {
            if(fig.getPosicao() == posicao){ return true; }
        }
        return false;
    }
}
